<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\PenyuluhController;
use App\Http\Controllers\JadwalController;
use App\Http\Controllers\MajelisController;
use App\Http\Controllers\LaporanController;
use App\Http\Controllers\AdminController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [HomeController::class, 'home']);

Route::get('/login', [LoginController::class, 'login'])->name('login')->middleware('guest');
Route::post('/login', [LoginController::class, 'authenticate']);
Route::post('/logout', [LoginController::class, 'logout']);

Route::get('/register', [RegisterController::class, 'register'])->middleware('guest');
Route::post('/register', [RegisterController::class, 'store']);

Route::get('/dashboard', [DashboardController::class, 'index'])->middleware('auth');

Route::resource('penyuluh', PenyuluhController::class);
Route::get('/penyuluh/search', [PenyuluhController::class, 'search']);

Route::resource('majelis', MajelisController::class);

Route::resource('jadwal', JadwalController::class);

Route::resource('laporan', LaporanController::class);

Route::resource('admin', AdminController::class);

// Route::get('/majelis', [MajelisController::class, 'index'])->name('majelis_view');
// Route::get('/majelis/create', [MajelisController::class, 'create'])->name('majelis_add');
// Route::post('/majelis/create', [MajelisController::class, 'store'])->name('majelis_add');
// Route::get('/user/show/{id}', [MajelisController::class, 'show'])->name('majelis_show');
// Route::post('/user/edit/{id}', 'UserController@edit')->name('user_edit');
// Route::post('/user/delete', 'UserController@delete')->name('user_delete');

// Route::get('/login', [AuthController::class, 'index'])->name('login');
// Route::post('/custom-signin', [AuthController::class, 'createSignin'])->name('signin.custom');


// Route::get('/register', [AuthController::class, 'signup'])->name('register');
// Route::post('/create-user', [AuthController::class, 'customSignup'])->name('user.registration');


// Route::get('/dashboard', [AuthController::class, 'dashboardView'])->name('dashboard');
// Route::get('/logout', [AuthController::class, 'logout'])->name('logout');
