<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login()
    {
        return view('login.v_login', [
            "title" => "Login",
            "active" => "login"
        ]);
    }

    public function authenticate(Request $request)
    {

        //    $credentials = $request->validate(
        //        [
        //         'email' => 'required|email:dns',
        //         'password' => 'required',
        //     ]);

        $request->validate([
            'email' => 'required|email:dns',
            'password' => 'required',
        ]);
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();
            return redirect()->intended('dashboard')
                ->withSuccess('Logged-in');
        }
        return redirect("login")->with('loginError','Credentials are wrong.');
    }

    public function logout(Request $request)
    {

        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }
}
