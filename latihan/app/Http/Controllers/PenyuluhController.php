<?php

namespace App\Http\Controllers;

use App\Models\PenyuluhModel;
use Illuminate\Http\Request;
use App\Http\Repository\PenyuluhRepository;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\Hash;
class PenyuluhController extends Controller{

    protected $penyuluhRepo;

    public function __construct()
    {
        $this->penyuluhRepo=new PenyuluhRepository();

    }

    //menampilkan penyuluh
    public function index(Request $request) {

        // dd(request('search'));

        $penyuluh = PenyuluhModel::latest()->with('user')->paginate(5);


        return view('penyuluh.index',[
            "title" => "Penyuluh",
            "active" =>"penyuluh"
        ], compact('penyuluh'))
            ->with('i',(request()->input('page', 1) - 1) * 5);
    }

    public function create() {

        $user = User::all();

        return view ('penyuluh.create',[
            "title" => "Penyuluh",
            "active" =>"penyuluh",
            "users" => $user
        ]);
    }

    public function store(Request $request) {

        $request->validate([
            'nik' => 'required|unique:penyuluh',
            'nama' => 'required',
            'alamat' => 'required',
            'email' => 'required|email|unique:penyuluh',
            'status' => 'required',
            'no_hp' => 'required',
            'tgl_lahir' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'password' => 'required|min:6',
            'level' => 'required',
        ]);

        try {

             // $data = $request->all();
        $user = new User();
        $user->nama = $request->nama;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->level = $request->level;
        $user->save();
        // dump($user);

        $penyuluh = new PenyuluhModel();
        $penyuluh->id_user = $user->id;
        $penyuluh->nama = $user->nama;
        $penyuluh->nik = $request->nik;
        $penyuluh->alamat = $request->alamat;
        $penyuluh->email = $user->email;
        $penyuluh->status = $request->status;
        $penyuluh->no_hp = $request->no_hp;
        $penyuluh->tgl_lahir = $request->tgl_lahir;


        if ($image = $request->file('image')) {
            $destinationPath = 'image/';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            // $data['image'] = "$profileImage";
            $penyuluh->image = "$profileImage";
        }
        $penyuluh->save();
        // dump($penyuluh);
        // dd('test');
        // $check = $this->penyuluhRepo->createPenyuluh($data);
        return redirect()->route('penyuluh.index')->with('success','Successfully!');

        } catch (Exception $e) {

            // \DB::rollback();

            return redirect()->route('penyuluh.index')->with('error','Failed!');
        }
    }

    public function show(PenyuluhModel $penyuluh) {

        return view ('penyuluh.show',[
            "title" => "Penyuluh",
            "active" =>"penyuluh"
        ], compact('penyuluh'));

    }

    public function edit(PenyuluhModel $penyuluh) {

        return view ('penyuluh.edit',[
            "title" => "Penyuluh",
            "active" =>"penyuluh",
        ], compact('penyuluh'));

    }

    public function update(Request $request, PenyuluhModel $penyuluh) {

        //

        $request->validate([
            'nama' => 'required',
            'alamat' => 'required',
            'email' => 'required|email',
            'status' => 'required',
            'no_hp' => 'required',
            'tgl_lahir' => 'required',
        ]);

        $dataPenyuluh = [
            'nama' => $request->nama,
            'alamat'=>$request->alamat,
            'email'=>$request->email,
            'status'=>$request->status,
            'no_hp' => $request->no_hp,
            'tgl_lahir' => $request->tgl_lahir,

        ];

        $penyuluh->update($dataPenyuluh);

        $dataUser = [
            'nama' => $request->nama,
            'email'=>$request->email,
            'password'=>Hash::make($request->password),
            'level'=>$request->level,
        ];

        User::where('id', $penyuluh->id_user)->update($dataUser);

        // $user = new User();
        // $user->nama = $request->nama;
        // $user->email = $request->email;
        // $user->password = Hash::make($request->password);
        // $user->level = $request->level;

        // $penyuluh = new PenyuluhModel();
        // $penyuluh->id_user = $user->id;
        // $penyuluh->nama = $user->nama;
        // $penyuluh->nik = $request->nik;
        // $penyuluh->alamat = $request->alamat;
        // $penyuluh->email = $user->email;
        // $penyuluh->status = $request->status;
        // $penyuluh->no_hp = $request->no_hp;
        // $penyuluh->tgl_lahir = $request->tgl_lahir;

        // if ($image = $request->file('image')) {
        //     $destinationPath = 'image/';
        //     $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
        //     $image->move($destinationPath, $profileImage);
        //     $penyuluh->image = "$profileImage";
        // }else{
        //     unset($penyuluh->image);
        // }

        // $penyuluh->user()->save();
        // $check = $this->penyuluhRepo->updatePenyuluh($data);
        return redirect()->route('penyuluh.index')->with('success','Successfully!');

    }

    public function destroy (PenyuluhModel $penyuluh) {

        $penyuluh->user()->delete();
        return redirect()->route('penyuluh.index')->with('success','Successfully!');
    }

    public function search(Request $request){

        $keyword = $request->search;
        $penyuluh = PenyuluhModel::where('alamat', 'like', "%" . $keyword . "%")->orWhere('nama', 'like', "%" . $keyword . "%")->orWhere('nik', 'like', "%" . $keyword . "%")->orWhere('status', 'like', "%" . $keyword . "%")->orWhere('no_hp', 'like', "%" . $keyword . "%")->paginate(5);
        return view('penyuluh.index',[
            "title" => "Penyuluh",
            "active" =>"penyuluh"
        ], compact('penyuluh'))
            ->with('i',(request()->input('page', 1) - 1) * 5);

    }

}
