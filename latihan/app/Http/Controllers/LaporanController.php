<?php

namespace App\Http\Controllers;

use App\Models\LaporanModel;
use Illuminate\Http\Request;
use App\Models\NewMajelisModel;
use App\Models\PenyuluhModel;
use Exception;
use Illuminate\Support\Facades\DB;

class LaporanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $laporan = LaporanModel::latest()->with('majelis')->with('penyuluh')->paginate(5);


        return view('laporan.index',[
            "title" => "Laporan",
            "active" =>"laporan"
        ], compact('laporan'))
            ->with('i',(request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('laporan.create',[
            "title" => "Laporan",
            "active" =>"laporan",
            "penyuluh" => PenyuluhModel::all(),
            "majelis"=> NewMajelisModel::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul_laporan' => 'required',
            'isi_kegiatan' => 'required',
            'jumlah_peserta' => 'required',
            'tanggal_kegiatan' => 'required',
            'id_majelis' => 'required',
            'id_penyuluh' => 'required',
        ]);
        try {

        $laporan = new LaporanModel();
        $laporan->judul_laporan = $request->judul_laporan;
        $laporan->isi_kegiatan = $request->isi_kegiatan;
        $laporan->jumlah_peserta = $request->jumlah_peserta;
        $laporan->tanggal_kegiatan = $request->tanggal_kegiatan;
        $laporan->id_majelis = $request->id_majelis;
        $laporan->id_penyuluh = $request->id_penyuluh;
        $laporan->save();



        return redirect()->route('laporan.index')->with('success','Successfully!');

        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('laporan.index')->with('error','Failed!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LaporanModel  $laporanModel
     * @return \Illuminate\Http\Response
     */
    public function show(LaporanModel $laporan)
    {
        return view ('laporan.show',[
            "title" => "Laporan",
            "active" =>"laporan"
        ], compact('laporan'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LaporanModel  $laporanModel
     * @return \Illuminate\Http\Response
     */
    public function edit(LaporanModel $laporan)
    {
        return view ('laporan.edit',[
            "title" => "Laporan",
            "active" =>"laporan",
            "penyuluh" => PenyuluhModel::all(),
            "majelis"=> NewMajelisModel::all(),
        ], compact('laporan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\LaporanModel  $laporanModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LaporanModel $laporan)
    {

        try {

            $request->validate([
                'judul_laporan' => 'required',
                'isi_kegiatan' => 'required',
                'jumlah_peserta' => 'required',
                'tanggal_kegiatan' => 'required',
                'id_majelis' => 'required',
                'id_penyuluh' => 'required',
            ]);

            $dataLaporan = [
                'judul_laporan' => $request->judul_laporan,
                'isi_kegiatan' => $request->isi_kegiatan,
                'jumlah_peserta' => $request->jumlah_peserta,
                'tanggal_kegiatan' => $request->tanggal_kegiatan,
                'id_majelis' => $request->id_majelis,
                'id_penyuluh' => $request->id_penyuluh,
            ];

            $laporan->update($dataLaporan);

            return redirect()->route('laporan.index')->with('success','Successfully!');
        }

        catch (\Exception $e) {
            return redirect()->route('laporan.index')->with('error','Failed!');
       }



        }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LaporanModel  $laporanModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(LaporanModel $laporan)
    {
        $laporan->delete();
        return redirect()->route('laporan.index')->with('success','Successfully!');
    }
}
