<?php

namespace App\Http\Controllers;

use App\Models\NewMajelisModel;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\DB;


class MajelisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $majelis = NewMajelisModel::first()->paginate(5);

        return view('majelis.index', [
            "title" => "Majelis",
            "active" => "majelis"
        ], compact('majelis'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('majelis.create', [
            "title" => "Majelis",
            "active" => "majelis",

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_majelis' => 'required',
            'alamat' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $data = $request->all();
        try {

            if ($image = $request->file('image')) {
                $destinationPath = 'image/';
                $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
                $image->move($destinationPath, $profileImage);
                $data['image'] = "$profileImage";
            }

            NewMajelisModel::create($data);

            return redirect()->route('majelis.index')->with('success', 'Successfully!');
        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('majelis.index')->with('error', 'Failed!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\NewMajelisController  $majelis
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $majelis = NewMajelisModel::find($id);

        return view('majelis.show',[
            "title" => "Majelis",
            "active" => "majelis"
          ],compact('majelis'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MajelisModel  $majelisModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $majelis = NewMajelisModel::find($id);

        return view('majelis.edit',[
            "title" => "Majelis",
            "active" => "majelis"
          ],compact('majelis'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MajelisModel  $majelisModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $majelis = NewMajelisModel::find($id);

        try {

            $request->validate([
                'nama_majelis' => 'required',
                'alamat' => 'required',
            ]);

            $dataMajelis = [
                'nama_majelis' => $request->nama_majelis,
                'alamat'=>$request->alamat,
            ];


        if ($image = $request->file('image')) {
            $destinationPath = 'image/';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $dataMajelis['image'] = "$profileImage";
        }else{
            unset($dataMajelis['image']);
        }

            $majelis->update($dataMajelis);

            return redirect()->route('majelis.index')->with('success','Successfully!');

        } catch (\Exception $e) {
             return redirect()->route('majelis.index')->with('error','Failed!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MajelisModel  $majelisModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $majelis = NewMajelisModel::find($id);

        $majelis->delete();
        return redirect()->route('majelis.index')->with('success','Successfully!');
    }
}
