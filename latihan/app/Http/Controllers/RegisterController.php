<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Repository\UserRepository;
use Illuminate\Support\Facades\Session;

class RegisterController extends Controller
{
    protected $userRepo;

    public function __construct()
    {
        $this->userRepo=new UserRepository();

    }
    public function register () {
        return view('register.v_register',[
            "title" => "Register",
            "active" =>"register"
        ]);
    }

    public function store(Request $request) {

        $request->validate([
            'nama' => 'req     uired',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'level' => 'required'
        ]);

        $data = $request->all();
        $check = $this->userRepo->createUser($data);
        return redirect("login")->withSuccess('Successfully logged-in!');

    }


}
