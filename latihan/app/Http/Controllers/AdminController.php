<?php

namespace App\Http\Controllers;

use App\Models\AdminModel;
use App\Models\User;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admin = AdminModel::latest()->with('user')->paginate(5);


        return view('admin.index',[
            "title" => "Admin",
            "active" =>"admin"
        ], compact('admin'))
            ->with('i',(request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
            $user = User::all();

            return view ('admin.create',[
                "title" => "admin",
                "active" =>"admin",
                "users" => $user
            ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'alamat' => 'required',
            'email' => 'required|email|unique:penyuluh',
            'no_hp' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'password' => 'required|min:6',
            'level' => 'required',
        ]);

        try {

        $user = new User();
        $user->nama = $request->nama;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->level = $request->level;
        $user->save();

        $admin = new AdminModel();
        $admin->id_user = $user->id;
        $admin->nama = $user->nama;
        $admin->alamat = $request->alamat;
        $admin->email = $user->email;
        $admin->no_hp = $request->no_hp;


        if ($image = $request->file('image')) {
            $destinationPath = 'image/';
            $profileImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $profileImage);
            $admin->image = "$profileImage";
        }


        $admin->save();
        return redirect()->route('admin.index')->with('success','Successfully!');

        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('admin.index')->with('error','Failed!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AdminModel  $adminModel
     * @return \Illuminate\Http\Response
     */
    public function show(AdminModel $admin)
    {
        return view ('admin.show',[
            "title" => "Admin",
            "active" =>"admin"
        ], compact('admin'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AdminModel  $adminModel
     * @return \Illuminate\Http\Response
     */
    public function edit(AdminModel $admin)
    {
        return view ('admin.edit',[
            "title" => "Admin",
            "active" =>"admin"
        ], compact('admin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AdminModel  $adminModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AdminModel $admin)
    {

        try {
            $request->validate([
                'nama' => 'required',
                'alamat' => 'required',
                'email' => 'required|email',
                'no_hp' => 'required',
            ]);

            $dataAdmin = [
                'nama' => $request->nama,
                'alamat'=>$request->alamat,
                'email'=>$request->email,
                'no_hp' => $request->no_hp,
            ];

            $admin->update($dataAdmin);

            $dataUser = [
                'nama' => $request->nama,
                'email'=>$request->email,
                'password'=>Hash::make($request->password),
                'level'=>$request->level,
            ];

            User::where('id', $admin->id_user)->update($dataUser);

            return redirect()->route('admin.index')->with('success','Successfully!');
        } catch (\Throwable $th) {

            DB::rollback();

            return redirect()->route('admin.index')->with('error','Failed!');

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AdminModel  $adminModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(AdminModel $admin)
    {
        $admin->user()->delete();
        return redirect()->route('penyuluh.index')->with('success','Successfully!');
    }
}
