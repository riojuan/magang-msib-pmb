<?php

namespace App\Http\Controllers;

use App\Models\JadwalModel;
use App\Models\NewMajelisModel;
use App\Models\PenyuluhModel;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class JadwalController extends Controller
{
    public function index(Request $request) {

        $jadwal = JadwalModel::latest()->with('majelis')->with('penyuluh')->paginate(5);


        return view('jadwal.index',[
            "title" => "Jadwal",
            "active" =>"jadwal"
        ], compact('jadwal'))
            ->with('i',(request()->input('page', 1) - 1) * 5);
    }

    public function create() {

        return view ('jadwal.create',[
            "title" => "Jadwal",
            "active" =>"jadwal",
            "penyuluh" => PenyuluhModel::all(),
            "majelis"=> NewMajelisModel::all(),
        ]);
    }

    public function store(Request $request) {

        $request->validate([
            'materi' => 'required',
            'lokasi' => 'required',
            'hari_jam' => 'required',
            'id_majelis' => 'required',
            'id_penyuluh' => 'required',
        ]);

        try {

        $jadwal = new JadwalModel();
        $jadwal->materi = $request->materi;
        $jadwal->lokasi = $request->lokasi;
        $jadwal->hari_jam = date("Y-m-d H:i:s", strtotime($request->hari_jam)) ;
        $jadwal->id_majelis = $request->id_majelis;
        $jadwal->id_penyuluh = $request->id_penyuluh;
        $jadwal->save();

        return redirect()->route('jadwal.index')->with('success','Successfully!');

        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('jadwal.index')->with('error','Failed!');
        }
    }

    public function show(JadwalModel $jadwal) {

        return view ('jadwal.show',[
            "title" => "Jadwal",
            "active" =>"jadwal"
        ], compact('jadwal'));

    }

    public function edit(JadwalModel $jadwal) {

        return view ('jadwal.edit',[
            "title" => "Jadwal",
            "active" =>"jadwal",
            "penyuluh" => PenyuluhModel::all(),
            "majelis"=> NewMajelisModel::all(),
        ], compact('jadwal'));

    }

    public function update(Request $request, JadwalModel $jadwal) {

        //

        $request->validate([
            'materi' => 'required',
            'lokasi' => 'required',
            'hari_jam' => 'required',
            'id_majelis' => 'required',
            'id_penyuluh' => 'required',
        ]);

        // date("Y-m-d H:i:s", strtotime($request->hari_jam));

        $dataJadwal = [
            'materi' => $request->materi,
            'lokasi' => $request->lokasi,
            'hari_jam' => date("Y-m-d H:i:s", strtotime($request->hari_jam)),
            'id_majelis' =>  $request->id_majelis,
            'id_penyuluh' =>  $request->id_penyuluh,
        ];


        $jadwal->update($dataJadwal);

        return redirect()->route('jadwal.index')->with('success','Successfully!');

    }



    public function destroy (JadwalModel $jadwal) {

        $jadwal->delete();
        return redirect()->route('jadwal.index')->with('success','Successfully!');
    }


}
