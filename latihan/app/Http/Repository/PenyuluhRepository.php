<?php

namespace App\Http\Repository;

use App\Models\PenyuluhModel;
use Illuminate\Support\Facades\Hash;

class PenyuluhRepository {

    public function createPenyuluh(array $data)
    {
      return PenyuluhModel::create([
        'id_user' => $data['id_user'],
        'nik' => $data['nik'],
        'nama' => $data['nama'],
        'alamat' => $data['alamat'],
        'email' => $data['email'],
        'status' => $data['status'],
        'no_hp' => $data['no_hp'],
        'tgl_lahir' => $data['tgl_lahir'],
        'image' => $data['image'],
      ]);


    }



}

?>
