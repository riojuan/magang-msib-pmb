<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PenyuluhModel extends Model
{
    use HasFactory;

    protected $table = 'penyuluh';

    protected $fillable = [
        'id_user',
        'nik',
        'nama',
        'alamat',
        'email',
        'status',
        'no_hp',
        'tgl_lahir',
        'image',
        'created_at',
        'updated_at'
    ];

    public function user() {
        return $this->belongsTo('App\Models\User', 'id_user', 'id');
    }

    public function jadwal() {
        return $this->hasMany('App\Models\JadwalModel','id_penyuluh', 'id');
    }

    public function laporan() {
        return $this->hasMany('App\Models\LaporanModel','id_penyuluh', 'id');
    }

}
