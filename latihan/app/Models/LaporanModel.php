<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LaporanModel extends Model
{
    use HasFactory;

    protected $table = 'laporan';

    protected $fillable = [
        'judul_laporan',
        'isi_kegiatan',
        'jumlah_peserta',
        'tanggal_kegiatan',
        'id_penyuluh',
        'id_majelis',
    ];

    public function penyuluh() {
        return $this->belongsTo('App\Models\PenyuluhModel','id_penyuluh', 'id');
    }
    public function majelis() {
        return $this->belongsTo('App\Models\NewMajelisModel','id_majelis','id');
    }

}
