<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NewMajelisModel extends Model
{
    use HasFactory;

    protected $table = 'majelis';

    protected $fillable = [
            'nama_majelis',
            'alamat',
            'image',
        ];

    public function jadwal() {
        return $this->hasMany('App\Models\JadwalModel','id_majelis','id');
    }

    public function laporan() {
        return $this->hasMany('App\Models\LaporanModel','id_majelis','id');
    }

}
