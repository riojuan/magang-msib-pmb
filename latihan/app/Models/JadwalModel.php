<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JadwalModel extends Model
{
    use HasFactory;

    protected $table = 'jadwal';

    protected $fillable = [
        'hari_jam',
        'materi',
        'lokasi',
        'id_majelis',
        'id_penyuluh',

    ];

    public function penyuluh () {
        return $this->belongsTo('App\Models\PenyuluhModel','id_penyuluh','id');
    }

    public function majelis() {
        return $this->belongsTo('App\Models\NewMajelisModel','id_majelis','id');
    }
}
