@extends('layouts.main')

@section('container')
    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>


<main class="form-register">
  <form action="/register" method ="post">
    @csrf
    <h1 class="h3 mb-3 fw-normal">Please sign up</h1>


    {{-- <div class="form-floating">
      <input type="text" name = "name" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="name" value="{{ old('name') }}">
      <label for="name">Name</label>
      @error('name')
      <div class="invalid-feedback">
        {{ $message }}
      </div>
      @enderror
    </div> --}}
    {{-- <div class="form-floating">
      <input type="text" name = "id_user" class="form-control @error('id_user') is-invalid @enderror" id="id_user" placeholder="id_user" value="{{ old('id_user') }}">
      <label for="id_user">ID</label>
      @error('id_user')
      <div class="invalid-feedback">
        {{ $message }}
      </div>
      @enderror
    </div> --}}

    <div class="form-floating">
        <input type="text" name = "name" class="form-control  @error('name') is-invalid @enderror" id="name" placeholder="name" value="{{ old('name') }}">
        <label for="name">name</label>
        @error('name')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
      </div>
      <div class="form-floating">
        <input type="email" name = "email" class="form-control @error('email') is-invalid @enderror" id="email" placeholder="email" value="{{ old('email') }}">
        <label for="email">Email</label>
        @error('email')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
      </div>
    <div class="form-floating">
      <input type="password" name = "password" class="form-control @error('password') is-invalid @enderror" id="floatingPassword" placeholder="Password">
      <label for="floatingPassword">Password</label>
      @error('password')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
    </div>

    <div class="form-floating">
        <input type="text" name = "level" class="form-control @error('level') is-invalid @enderror" id="level" placeholder="level" value="{{ old('level') }}">
        <label for="level">Level</label>
        @error('level')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
      </div>



    <button class="w-100 btn btn-lg btn-primary mt-4" type="submit">Sign up</button>

  </form>
</main>
@endsection

