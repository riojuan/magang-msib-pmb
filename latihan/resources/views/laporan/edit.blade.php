@extends('layouts.main')

@section('container')

    @if (session()->has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('success') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif

    @if (session()->has('error'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {{ session('loginError') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Edit Laporan</h2>
        </div>
        <div class="pull-right mt-4">
            <a class="btn btn-primary" href="{{ route('laporan.index') }}"> Back</a>
        </div>
    </div>
</div>

<form action="{{ route('laporan.update', $laporan->id) }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                {{-- <strong>Id User:</strong> --}}
                <input type="hidden" name="id" value="{{ $laporan->id }}" class="form-control" placeholder="id">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Penyuluh:</strong>
                <select name="id_penyuluh">
                    @foreach ($penyuluh as $p)
                    <option value="{{ $p->id }}">{{ $p->nama }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Majelis:</strong>
                <select name="id_majelis">
                    @foreach ($majelis as $m)
                    <option value="{{ $m->id }}">{{ $m->nama_majelis }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Judul</strong>
                <input type="text" name="judul_laporan"  value="{{ $laporan->judul_laporan }}" class="form-control" placeholder="Judul">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Isi:</strong>
                <input type="text" name="isi_kegiatan"  value="{{ $laporan->isi_kegiatan }}"class="form-control" placeholder="materi">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Jumlah Peserta:</strong>
                <input type="number" name="jumlah_peserta"  value="{{ $laporan->jumlah_peserta }}"class="form-control" placeholder="Jumlah Peserta">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Tanggal:</strong>
                <input type="date" name="tanggal_kegiatan"  value="{{ $laporan->tanggal_kegiatan }}"class="form-control" placeholder="">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>

</form>

@endsection
