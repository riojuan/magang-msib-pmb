@extends('layouts.main')

@section('container')

@if (session()->has ('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    {{ session('success') }}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  </div>
@endif

@if (session()->has ('error'))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    {{ session('loginError') }}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  </div>
@endif

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Laporan</h2>
        </div>
        <div class="pull-right mt-5 mb-4">
            <a class="btn btn-success" href="{{ route('laporan.create') }}"> Tambah</a>
        </div>
    </div>
</div>

<form class="form" method="get" action="#">
    @csrf
    <div class="form-group w-100 mb-3">
        <label for="search" class="d-block mr-2">Pencarian</label>
        <input type="text" name="search" class="form-control w-75 d-inline" id="search" placeholder="Masukkan keyword">
        <button type="submit" class="btn btn-primary mb-1">Cari</button>
    </div>
</form>

<table class="table table-bordered">
    <tr>
        <th>No</th>
        <th>Judul</th>
        <th>Nama Penyuluh</th>
        <th>Isi</th>
        <th>Jumlah Peserta</th>
        <th>Majelis</th>
        <th>Tanggal</th>
        <th width="280px">Aksi</th>
    </tr>
    @foreach ($laporan as $l)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $l->judul_laporan}}</td>
        <td>{{ $l->penyuluh->nama }}</td>
        <td>{{ $l->isi_kegiatan }}</td>
        <td>{{ $l->jumlah_peserta }}</td>
        <td>{{ $l->majelis->nama_majelis }}</td>
        <td>{{ $l->tanggal_kegiatan }}</td>
        <td>
            <form action="{{ route('laporan.destroy',$l->id) }}" method="post">

                <a class="btn btn-info" href="{{ route('laporan.show', $l->id) }}">Show</a>

                <a class="btn btn-primary" href="{{ route('laporan.edit', $l->id) }}">Edit</a>

                @csrf
                @method('DELETE')

                <button type="submit" class="btn btn-danger">Delete</button>
            </form>
        </td>
    </tr>
    @endforeach
</table>

{!! $laporan->links() !!}


@endsection
