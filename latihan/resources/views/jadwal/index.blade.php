@extends('layouts.main')

@section('container')

@if (session()->has ('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    {{ session('success') }}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  </div>
@endif

@if (session()->has ('error'))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    {{ session('loginError') }}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  </div>
@endif

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Jadwal</h2>
        </div>
        <div class="pull-right mt-5 mb-4">
            <a class="btn btn-success" href="{{ route('jadwal.create') }}"> Tambah</a>
        </div>
    </div>
</div>

<form class="form" method="get" action="#">
    @csrf
    <div class="form-group w-100 mb-3">
        <label for="search" class="d-block mr-2">Pencarian</label>
        <input type="text" name="search" class="form-control w-75 d-inline" id="search" placeholder="Masukkan keyword">
        <button type="submit" class="btn btn-primary mb-1">Cari</button>
    </div>
</form>

<table class="table table-bordered">
    <tr>
        <th>No</th>
        <th>Hari/jam</th>
        <th>Materi</th>
        <th>Lokasi</th>
        <th>Majelis</th>
        <th>Penyuluh</th>
        <th width="280px">Aksi</th>
    </tr>
    @foreach ($jadwal as $j)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $j->hari_jam }}</td>
        <td>{{ $j->materi }}</td>
        <td>{{ $j->lokasi }}</td>
        <td>{{ $j->majelis->nama_majelis }}</td>
        <td>{{ $j->penyuluh->nama }}</td>
        <td>
            <form action="{{ route('jadwal.destroy',$j->id) }}" method="post">

                <a class="btn btn-info" href="{{ route('jadwal.show',$j->id) }}">Show</a>

                <a class="btn btn-primary" href="{{ route('jadwal.edit',$j->id) }}">Edit</a>

                @csrf
                @method('DELETE')

                <button type="submit" class="btn btn-danger">Delete</button>
            </form>
        </td>
    </tr>
    @endforeach
</table>

{!! $jadwal->links() !!}


@endsection
