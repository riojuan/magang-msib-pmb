@extends('layouts.main')

@section('container')

    @if (session()->has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('success') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif

    @if (session()->has('error'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {{ session('loginError') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Edit Penyuluh</h2>
        </div>
        <div class="pull-right mt-4">
            <a class="btn btn-primary" href="{{ route('jadwal.index') }}"> Back</a>
        </div>
    </div>
</div>

<form action="{{ route('jadwal.update',$jadwal->id) }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                {{-- <strong>Id User:</strong> --}}
                <input type="hidden" name="id" value="{{ $jadwal->id }}" class="form-control" placeholder="id">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Penyuluh:</strong>
                <select name="id_penyuluh">
                    @foreach ($penyuluh as $p)
                    <option value="{{ $p->id }}">{{ $p->nama }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Majelis:</strong>
                <select name="id_majelis">
                    @foreach ($majelis as $m)
                    <option value="{{ $m->id }}">{{ $m->nama_majelis }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Hari Jam</strong>
                <input type="datetime-local" name="hari_jam"  value="{{ date("Y-m-d\TH:i:s", strtotime("$jadwal->hari_jam")) }}" class="form-control" placeholder="">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Materi:</strong>
                <input type="text" name="materi"  value="{{ $jadwal->materi }}"class="form-control" placeholder="materi">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Lokasi:</strong>
                <input type="text" name="lokasi"  value="{{ $jadwal->lokasi }}"class="form-control" placeholder="lokasi">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>

</form>

@endsection
