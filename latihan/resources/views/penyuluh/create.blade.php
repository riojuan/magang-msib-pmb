@extends('layouts.main')

@section('container')


    @if (session()->has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('success') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif

    @if (session()->has('error'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {{ session('loginError') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Tambah Penyuluh</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('penyuluh.index') }}"> Back</a>
            </div>
        </div>
    </div>

    <form action="{{ route('penyuluh.store') }}" method="POST" enctype="multipart/form-data">
        @csrf

        <div class="row">
            {{-- <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Id User:</strong>
                    <input type="number" name="id_user" class="form-control @error('id-user') is-invalid @enderror" placeholder="id_user" value="{{ old('id-user') }} required">

                    @error('id-user')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror

                </div>
            </div> --}}
            {{-- <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Id User:</strong>
                    <select>
                        @foreach ($users as $u)
                        <option value="{{ $u->id }}"></option>
                        @endforeach
                    </select>
                </div>
            </div> --}}

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Nama:</strong>
                    <input type="text" name="nama" class="form-control" placeholder="nama">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Nik:</strong>
                    <input type="text" name="nik" class="form-control" placeholder="nik">
                </div>
            </div>
            <div class="form-floating">
      <input type="password" name = "password" class="form-control @error('password') is-invalid @enderror" id="floatingPassword" placeholder="Password">
      <label for="floatingPassword">Password</label>
      @error('password')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
    </div>

    <div class="form-floating">
        <input type="text" name = "level" class="form-control @error('level') is-invalid @enderror" id="level" placeholder="level" value="{{ old('level') }}">
        <label for="level">Level</label>
        @error('level')
        <div class="invalid-feedback">
          {{ $message }}
        </div>
        @enderror
      </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Alamat:</strong>
                    <input type="text" name="alamat" class="form-control" placeholder="alamat">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Email:</strong>
                    <input type="text" name="email" class="form-control" placeholder="email">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Status:</strong>
                    <input type="text" name="status" class="form-control" placeholder="status">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Handphone:</strong>
                    <input type="text" name="no_hp" class="form-control" placeholder="no_hp">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Tanggal Lahir:</strong>
                    <input type="date" name="tgl_lahir" class="form-control" placeholder="tgl_lahir">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Foto:</strong>
                    <img class="img-preview img-fluid">
                    <input type="file" name="image" id ="image" class="form-control" placeholder="image" onchange="previewImage">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center mt-4">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>

    </form>

<script>
     function previewImage() {
        const image = document.querySelector('#image');
        const imgPreview = document.querySelector('.img-preview');

        imgPreview.style.display ='block';

        const oFReader = new FileReader();
        oFReader.readAsDataURL(image.files[0]);

        oFReader.onload = function(oFREvent){
            imgPreview.src = oFREvent.target.result;
        }
    }
</script>



@endsection
