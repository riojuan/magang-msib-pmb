<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Jadwal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jadwal', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_penyuluh');
            $table->unsignedBigInteger('id_majelis');
            $table->dateTime('hari_jam');
            $table->string('materi');
            $table->string('lokasi');
            $table->timestamps();
            $table->foreign('id_penyuluh')->references('id')->on('penyuluh')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('id_majelis')->references('id')->on('majelis')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
