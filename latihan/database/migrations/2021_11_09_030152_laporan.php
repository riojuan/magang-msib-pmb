<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Laporan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laporan', function (Blueprint $table) {
            $table->id();
            $table->string('judul_laporan');
            $table->string('isi_kegiatan');
            $table->integer('jumlah_peserta');
            $table->date('tanggal_kegiatan');
            $table->unsignedBigInteger('id_penyuluh');
            $table->unsignedBigInteger('id_majelis');
            $table->timestamps();
            $table->foreign('id_penyuluh')->references('id')->on('penyuluh')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('id_majelis')->references('id')->on('majelis')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()

    {
        Schema::dropIfExists('laporan');
    }
}
