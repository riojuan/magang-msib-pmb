<?php

namespace Database\Factories;

use App\Models\PenyuluhModel;
use Illuminate\Database\Eloquent\Factories\Factory;

class PenyuluhModelFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PenyuluhModel::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
